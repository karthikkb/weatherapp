package com.example.weatherapp

import android.app.Application
import androidx.room.Room
import com.androidnetworking.AndroidNetworking
import com.example.weatherapp.repository.local.MyAppDataBase
import timber.log.Timber


class MyApplication : Application() {

    companion object {
        lateinit var myAppContext: MyApplication
        lateinit var db: MyAppDataBase
    }

    override fun onCreate() {
        super.onCreate()
        myAppContext = this

        db = Room.databaseBuilder(
            this,
            MyAppDataBase::class.java, "History"
        ).allowMainThreadQueries().build()


        AndroidNetworking.initialize(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            //            Timber.plant(new ReleaseTree());
        }

    }

}
