package com.example.weatherapp.activity

import androidx.lifecycle.MutableLiveData
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.weatherapp.BuildConfig
import com.example.weatherapp.base.BaseViewModel
import com.example.weatherapp.repository.local.DataItem
import com.example.weatherapp.repository.local.DataOperations
import com.example.weatherapp.repository.model.WeatherForecastResult
import com.example.weatherapp.repository.model.WeatherResult
import com.example.weatherapp.utils.log.Logger
import com.google.gson.Gson
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.json.JSONObject


class MainViewModel : BaseViewModel() {

    var mWeatherResult = MutableLiveData<WeatherResult>()
    var mWeatherForecastResult = MutableLiveData<WeatherForecastResult>()

    fun getCurrentWeather(lat: String,long: String) {
        isLoading.value = true
        AndroidNetworking.get(BuildConfig.BASE_URL)
            .addPathParameter("type","weather")
            .addQueryParameter("lat", lat)
            .addQueryParameter("lon", long)
            .addQueryParameter("appid", BuildConfig.API_KEY)
            .addQueryParameter("units", "metric")
            .setTag("getCurrentWeather")
            .setPriority(Priority.HIGH)
            .build()
            .getAsObject(WeatherResult::class.java,object : ParsedRequestListener<WeatherResult>{
                override fun onResponse(response: WeatherResult?) {
                    Logger.d("data response", "" + Gson().toJson(response))
//                    mWeatherResult.value = response
                    runBlocking {
                        var isData = DataOperations.getExistedData("getCurrentWeather")
                        if(isData!==null)
                        {
                            isData.data = Gson().toJson(response)
                            DataOperations.update(isData)
                        }else {
                            var data = DataItem("getCurrentWeather", Gson().toJson(response))
                            DataOperations.insert(data)
                        }
                        loadCurrentWeather()
                        isLoading.value = false
                    }
                }

                override fun onError(anError: ANError?) {
                    Logger.d("data anError", "" + anError!!.message)
                    loadCurrentWeather()
                    isLoading.value = false
                }

            })
    }

    fun get5daysWeather(lat: String,long: String) {
        AndroidNetworking.get(BuildConfig.BASE_URL)
            .addPathParameter("type","forecast")
            .addQueryParameter("lat", lat)
            .addQueryParameter("lon", long)
            .addQueryParameter("appid", BuildConfig.API_KEY)
            .addQueryParameter("units", "metric")
            .setTag("get5daysWeather")
            .setPriority(Priority.HIGH)
            .build()
            .getAsObject(WeatherForecastResult::class.java,object : ParsedRequestListener<WeatherForecastResult>{
                override fun onResponse(response: WeatherForecastResult?) {
                    Logger.d("data response", "" + Gson().toJson(response))
//                    mWeatherForecastResult.value = response
                    runBlocking {
                        var isData = DataOperations.getExistedData("get5daysWeather")
                        if(isData!==null)
                        {
                            isData.data = Gson().toJson(response)
                            DataOperations.update(isData)
                        }else {
                            var data = DataItem("get5daysWeather", Gson().toJson(response))
                            DataOperations.insert(data)
                        }
                        load5daysWeather()
                    }
                }


                override fun onError(anError: ANError?) {
                    Logger.d("data anError", "" + anError!!.message)
                    load5daysWeather()
                }

            })
    }

    fun loadCurrentWeather() {

        runBlocking {
            val data = async { DataOperations.getExistedData("getCurrentWeather") }.await()
            data?.let {
                mWeatherResult.value = Gson().fromJson(data.data,WeatherResult::class.java)
            }
        }
    }

    fun load5daysWeather() {

        runBlocking {
            val data = async { DataOperations.getExistedData("get5daysWeather") }.await()
            data?.let {
                mWeatherForecastResult.value = Gson().fromJson(data.data,WeatherForecastResult::class.java)
            }
        }
    }

}