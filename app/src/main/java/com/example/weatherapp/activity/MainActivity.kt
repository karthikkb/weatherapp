package com.example.weatherapp.activity

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.R
import com.example.weatherapp.adapter.WeatherForecastAdapter
import com.example.weatherapp.base.BaseActivity
import com.example.weatherapp.databinding.ActivityMainBinding
import com.example.weatherapp.repository.local.DataOperations
import com.example.weatherapp.repository.model.WeatherResult
import com.example.weatherapp.utils.display.Common
import com.example.weatherapp.utils.display.SnackBar
import com.example.weatherapp.utils.log.Logger
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.squareup.picasso.Picasso
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.lang.StringBuilder


class MainActivity : BaseActivity<ActivityMainBinding,MainViewModel>(){
    override val bindingVariable: Int
        get() = -1
    override val layoutId: Int
        get() = R.layout.activity_main
    override val viewModel: MainViewModel
        get() = ViewModelProviders.of(
            this
        ).get(MainViewModel::class.java)

    //location callback
    lateinit var locationCallBack : LocationCallback
    //location request
    lateinit var locationRequest : LocationRequest
    //location
    lateinit var currentlocation : Location
    //Location provider intialization
    val fusedLocation : FusedLocationProviderClient by lazy { LocationServices.getFusedLocationProviderClient(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        runBlocking {
            DataOperations.getAlldata()
        }

        viewModel.loadCurrentWeather()
        viewModel.load5daysWeather()

        //recyclerview initialization
        mViewDataBinding.rv.setHasFixedSize(true)
        mViewDataBinding.rv.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)

        //current weather onbserver
        mViewModel.mWeatherResult.observe(this, Observer {

            mViewDataBinding.llWhole.visibility = (View.VISIBLE)

            mViewDataBinding.tvPlace.setText("Weather in "+it.name)
            mViewDataBinding.tvTemp.setText("${it.main.temp}° C")
            mViewDataBinding.tvDatetime.setText(Common.unixTimeStamptoDateTime(it.dt))

            Picasso.get().load(Common.getImage(it.weather.get(0).icon)).into(mViewDataBinding.imgWeather)

            var data = StringBuilder()
            data.append("Wind         :  Speed -"+it.wind.speed)
            data.append("    Deg -"+it.wind.deg+"\n")
            data.append("Pressure  :  "+it.main.pressure+" hpa\n")
            data.append("Humidity  :  "+it.main.humidity+" %\n")
            data.append("Sunrise     :  "+Common.unixTimeStamptoHour(it.sys.sunrise)+"\n")
            data.append("Sunset      :  "+Common.unixTimeStamptoHour(it.sys.sunset)+"\n")
//            data.append("Geo coords : "+1+"\n")


            mViewDataBinding.tvData.setText(data)

        })

        //current 5 days forcast onbserver
        mViewModel.mWeatherForecastResult.observe(this, Observer {
            val adapter = WeatherForecastAdapter(it.list)
            mViewDataBinding.rv.adapter = adapter
        })

        //check permission
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if(report.areAllPermissionsGranted())
                    {
                        buildLocationRequest()
                        buildLocationCallBack()

                        fusedLocation.requestLocationUpdates(locationRequest, locationCallBack, Looper.myLooper())
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                    SnackBar.show(mViewDataBinding.constraintlayout,"Permission Denied")
                }
            }).check()
    }

    private fun buildLocationCallBack() {
        locationCallBack = object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                currentlocation = locationResult!!.lastLocation
                Logger.d("currentlocation",""+currentlocation.latitude+" / "+currentlocation.longitude)

                 viewModel.getCurrentWeather(""+currentlocation.latitude,""+currentlocation.longitude)
                 viewModel.get5daysWeather(""+currentlocation.latitude,""+currentlocation.longitude)

            }
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setSmallestDisplacement(10.0f)
    }

}
