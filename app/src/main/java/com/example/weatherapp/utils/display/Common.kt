package com.example.weatherapp.utils.display

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView

import androidx.core.content.ContextCompat
import com.example.weatherapp.MyApplication
import com.example.weatherapp.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Common {

    fun getImage( icon : String) : String {
        return String.format("http://openweathermap.org/img/wn/%s.png",icon)
    }

    fun unixTimeStamptoDateTime( unixTimeStamp: Long) :  String {
        var dateFormat = SimpleDateFormat(" dd MMM yyyy   EEE HH:mm ")
        var date = Date()
        date.time = (unixTimeStamp * 1000L)
        return dateFormat.format(date)

    }

    fun unixTimeStamptoHour( unixTimeStamp: Long) :  String {
        var dateFormat = SimpleDateFormat("HH:mm")
        var date = Date()
        date.time = (unixTimeStamp * 1000L)
        return dateFormat.format(date)

    }

    fun getDateNow( text: String) : String {
        var dateFormat = SimpleDateFormat("dd MMMM yyyy HH:mm")
        var date = Date()
        return dateFormat.format(date)
    }


}
