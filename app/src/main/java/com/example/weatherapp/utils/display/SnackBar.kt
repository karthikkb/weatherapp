package com.example.weatherapp.utils.display

import android.graphics.Color
import android.view.View
import android.widget.TextView

import com.google.android.material.snackbar.Snackbar

object SnackBar {

    fun show(view: View, text: String) {
        val snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG)
        snackbar.show()
    }

    fun customShow(view: View, text: String) {
        val snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG)
        //                .setAction("RETRY", new View.OnClickListener() {
        //                    @Override
        //                    public void onClick(View view) {
        //                    }
        //                });
        snackbar.setActionTextColor(Color.RED)
        val sbView = snackbar.view
        val textView =
            sbView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.YELLOW)
        snackbar.show()
    }

}
