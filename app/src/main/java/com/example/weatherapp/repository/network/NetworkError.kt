package com.example.weatherapp.repository.network


class NetworkError {
     var status = -1
     var statusCode = "-1"
     var message = "Something went wrong"
}
