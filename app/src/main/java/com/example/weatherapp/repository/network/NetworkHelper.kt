package com.example.weatherapp.repository.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import com.example.weatherapp.MyApplication
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.*

class NetworkHelper private constructor() {

    private val TAG = "NetworkHelper"

    companion object {
        lateinit var connectivityManager: ConnectivityManager

        init {
            connectivityManager =
                MyApplication.myAppContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }

        fun isNetworkConnected(): Boolean {
            var result = false
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val networkCapabilities = connectivityManager.activeNetwork ?: return false
                val actNw =
                    connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
                result = when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } else {
                @Suppress("DEPRECATION")
                connectivityManager.run {
                    connectivityManager.activeNetworkInfo?.run {
                        result = when (type) {
                            ConnectivityManager.TYPE_WIFI -> true
                            ConnectivityManager.TYPE_MOBILE -> true
                            ConnectivityManager.TYPE_ETHERNET -> true
                            else -> false
                        }

                    }
                }
            }

            return result
        }


        fun isInternetAvailable(context: Context): Boolean {
            if (isNetworkConnected()) {
                var inetAddress: InetAddress? = null
                try {
                    val future = Executors.newSingleThreadExecutor().submit(Callable<InetAddress> {
                        try {
                            return@Callable InetAddress.getByName("google.com")
                        } catch (e: UnknownHostException) {
                            return@Callable null
                        }
                    })
                    inetAddress = future.get(2000, TimeUnit.MILLISECONDS)
                    future.cancel(true)
                } catch (e: InterruptedException) {
                } catch (e: ExecutionException) {
                } catch (e: TimeoutException) {
                }

                return (inetAddress != null)
            } else
                return false
        }

        fun getNetworkType(): Int {
            val activeNetworkInfo = connectivityManager?.activeNetworkInfo
            return activeNetworkInfo?.type ?: 0

        }
    }


    //    fun castToNetworkError(throwable: Throwable): NetworkError {
    //        val defaultNetworkError = NetworkError()
    //        try {
    //            if (throwable is ConnectException) return NetworkError(0, "0")
    //            if (throwable !is HttpException) return defaultNetworkError
    //            val error = GsonBuilder()
    //                    .excludeFieldsWithoutExposeAnnotation()
    //                    .create()
    //                .fromJson(throwable.response().errorBody()?.string(), NetworkError::class.java)
    //            return NetworkError(throwable.code(), error.statusCode, error.message)
    //        } catch (e: IOException) {
    //            Logger.e(TAG, e.toString())
    //        } catch (e: JsonSyntaxException) {
    //            Logger.e(TAG, e.toString())
    //        } catch (e: NullPointerException) {
    //            Logger.e(TAG, e.toString())
    //        }
    //        return defaultNetworkError
    //    }
}
