package com.example.weatherapp.repository.local

import androidx.room.*

@Dao
interface DAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert( docItem: DataItem )

    @Update
    suspend fun update( docItem: DataItem )

    @Delete
    suspend fun delete( docItem: DataItem )

    @Query("SELECT * FROM Data")
    suspend fun getAlldata(): List<DataItem>

    @Query("SELECT * FROM Data WHERE title=:title")
    suspend fun getExistedData(title : String): DataItem


}