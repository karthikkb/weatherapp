/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/////////////////////////////////////////////////////////////
// QueueItem: 
//	This is a single event (item) in the Event Queue
//  It is used as the basis for encoding an outgoing raw data message (done in Codec)
//	It is also used as the result for a decoded incoming messages from server (also done in Codec)

package com.example.weatherapp.repository.local
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Data")
data class DataItem(var title  : String = "",
                    var data : String = "") {
    //  ID is a unique ID for the item, different from the server_id or sequence_id
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
