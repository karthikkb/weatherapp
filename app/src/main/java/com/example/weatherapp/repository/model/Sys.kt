package com.example.weatherapp.repository.local

data class Sys (

	val country : String,
	val sunrise : Long,
	val sunset : Long
)