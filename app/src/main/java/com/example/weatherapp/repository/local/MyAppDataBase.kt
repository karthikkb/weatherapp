package com.example.weatherapp.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [DataItem::class], version = 1, exportSchema = false)
abstract class MyAppDataBase : RoomDatabase() {
    abstract fun dao(): DAO
}