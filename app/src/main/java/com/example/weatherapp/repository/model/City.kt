package com.example.weatherapp.repository.local

data class City (
	val id : Int,
	val name : String,
	val coord : Coord,
	val country : String
)