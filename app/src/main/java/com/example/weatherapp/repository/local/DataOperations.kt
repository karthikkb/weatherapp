package com.example.weatherapp.repository.local


import android.util.Log
import com.example.weatherapp.MyApplication
import com.example.weatherapp.utils.log.Logger
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

object DataOperations {

     suspend fun insert(dataItem: DataItem): DataItem {
//         runBlocking {
//             CoroutineScope(Dispatchers.IO).async {
                 MyApplication.db.dao().insert(dataItem)
//             }.await()
//         }
        return dataItem
    }

     fun update(dataItem: DataItem): DataItem {
        runBlocking {
            CoroutineScope(Dispatchers.IO).async {
                MyApplication.db.dao().update(dataItem)
            }.await()
        }
        return dataItem
    }

     fun deleteItem(dataItem: DataItem) {
        runBlocking {
            CoroutineScope(Dispatchers.IO).async {
                MyApplication.db.dao().delete(dataItem)
            }.await()
        }
    }

     suspend fun getAlldata(): List<DataItem>? {
        var data : List<DataItem>? =null
                data = MyApplication.db.dao().getAlldata()
                Log.d("getAlldata",""+data!!.size)

        return data
    }

     fun getExistedData(title :  String): DataItem? {
        var data : DataItem? =
        runBlocking {
            CoroutineScope(Dispatchers.IO).async {
                MyApplication.db.dao().getExistedData(title)
            }.await()
        }
        return data
    }

}
