package com.example.weatherapp.repository.local

data class Coord (

	val lon : Double,
	val lat : Double
)