package com.example.weatherapp.repository.model

import java.io.Serializable

class Page (
    var pageid: Int,
    var ns: Int,
    var title: String="",
    var index: Int,
    var contentmodel: String?,
    var pagelanguage: String?,
    var pagelanguagehtmlcode: String?,
    var pagelanguagedir: String?,
    var touched: String?,
    var lastrevid: Long,
    var length: Long,
    var fullurl: String?,
    var editurl: String?,
    var canonicalurl: String?

): Serializable{

}
