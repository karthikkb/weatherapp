package com.example.weatherapp.repository.model

import com.example.weatherapp.repository.local.*


data class MyList (
    val rain : Rain,
    val weather : List<Weather>,
    val main : Main,
    val wind : Wind,
    val clouds : Clouds,
    val dt : Long,
    val sys : Sys
)