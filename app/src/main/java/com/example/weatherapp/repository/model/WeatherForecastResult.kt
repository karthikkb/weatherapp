package com.example.weatherapp.repository.model

import com.example.weatherapp.repository.local.*


data class WeatherForecastResult (

    val list : List<MyList>,
    val message : Double,
    val cnt : Int,
    val cod : Int,
    val city : City
)