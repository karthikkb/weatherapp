package com.example.weatherapp.repository.local

data class Wind (

	val speed : Double,
	val deg : Int
)