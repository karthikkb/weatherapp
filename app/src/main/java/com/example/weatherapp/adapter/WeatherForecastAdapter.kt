package com.example.weatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.databinding.ListItemBinding
import com.example.weatherapp.repository.model.MyList
import com.example.weatherapp.repository.model.WeatherForecastResult
import com.example.weatherapp.utils.display.Common
import com.squareup.picasso.Picasso
import java.util.*

class WeatherForecastAdapter(var weatherList: List<MyList> ) : RecyclerView.Adapter<WeatherForecastAdapter.MyViewHolder>() {


    private var layoutInflater: LayoutInflater? = null

    class MyViewHolder(val binding: ListItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ListItemBinding>(
                layoutInflater!!,
                R.layout.list_item,
                parent,
                false
            )
            return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = weatherList.get(position)
        Picasso.get().load(Common.getImage(data.weather.get(0).icon)).into(holder.binding.imgWeather)
        holder.binding.tvDate.setText(Common.unixTimeStamptoDateTime(data.dt))
        holder.binding.tvTemp.setText( "${data.main.temp}° C")
        holder.binding.tvDes.setText(data.weather.get(0).description)
    }

}