package com.example.weatherapp.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isKeyboardHide: MutableLiveData<Boolean> = MutableLiveData()
    val toastMessageId: MutableLiveData<Int> = MutableLiveData()
    val toastMessageString: MutableLiveData<String> = MutableLiveData()
    val alertMessageId: MutableLiveData<Int> = MutableLiveData()
    val alertMessageString: MutableLiveData<String> = MutableLiveData()

    val compositeDisposable: CompositeDisposable
    //    private WeakReference<N> mNavigator;

    init {
        this.compositeDisposable = CompositeDisposable()
        setIsKeyboardHide(false)
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun setIsLoading(isLoading: Boolean) {
        this.isLoading.value = isLoading
    }

    fun setIsKeyboardHide(isKeyboardHide: Boolean) {
        this.isKeyboardHide.value = isKeyboardHide
    }


}
