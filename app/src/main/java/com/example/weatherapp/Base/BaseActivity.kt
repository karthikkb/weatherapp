package com.example.weatherapp.base

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.example.weatherapp.repository.network.NetworkHelper
import com.example.weatherapp.utils.display.Alert
import com.example.weatherapp.utils.display.CustomProgressBar
import com.example.weatherapp.utils.display.Toaster
import com.example.weatherapp.utils.log.Logger


abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
    protected lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V
    private val progressBar = CustomProgressBar()

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V

    val isNetworkConnected: Boolean
        get() = NetworkHelper.isInternetAvailable(applicationContext)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.e("lifecycle","onCreate")
        performDataBinding()
        initObservables()
    }

    override fun onPause() {
        super.onPause()
        Logger.e("lifecycle","onPause")
    }

    override fun onResume() {
        super.onResume()
        Logger.e("lifecycle","onResume")
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun hideLoading() {
        progressBar.dialog?.let {
            if(it.isShowing)
                it.dismiss() }

    }

    fun openActivityOnTokenExpire() {
        finish()
    }


    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    fun showLoading() {
        hideLoading()
        progressBar.show(this, null)

    }

    fun showLoading(message: String?) {
        hideLoading()
        progressBar.show(this, message)
    }

    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, layoutId)
        this.mViewModel = viewModel
        mViewDataBinding.setVariable(bindingVariable, mViewModel)
        mViewDataBinding.executePendingBindings()
    }


    protected fun initObservables() {

        mViewModel.isKeyboardHide.observe(this, Observer { status ->
            Logger.e("isLoading.observe", "", "" + status!!)
            if (status) {
                hideKeyboard()
            }
        })

        mViewModel.isLoading.observe(this, Observer { status ->
            Logger.e("isLoading.observe", "", "" + status!!)
            if (status) {
                showLoading()
            } else {
                hideLoading()
            }
        })

        mViewModel.toastMessageId.observe(this, Observer { integer -> toastMessage(integer) })

        mViewModel.toastMessageString.observe(this, Observer { s -> toastMessage(s) })

        mViewModel.alertMessageId.observe(this, Observer { integer -> alertMessage(integer) })

        mViewModel.alertMessageString.observe(this, Observer { s -> alertMessage(s) })

    }

    fun toastMessage(message: Any?) {
        if (message is String) {
            Toaster.show(this, message)
        } else if (message is Int) {
            Toaster.show(this, message)
        }

    }

    fun alertMessage(message: Any?) {
        if (message is String) {
            Alert.showDialog(this, message)
        } else if (message is Int) {
            Alert.showDialog(this, message)
        }

    }

//    fun progressbar(message: String) {
//        //Progress Bar with Text
//        progressBar.show(this, message)
//
//        //Progress Bar without Text
//        progressBar.show(this)
//    }

    override fun onDestroy() {
        super.onDestroy()
    }

}
